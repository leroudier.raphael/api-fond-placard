import { Ingredient } from '../business/Ingredient'
import { IngredientService } from '../services/IngredientService';

export async function createIngredient(req, res, next) {
    let x: Ingredient = req['body']['name']
    IngredientService.save(x).then(
        (x: Ingredient) => { res.status(201).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function getOneIngredient(req, res, next) {
    const id: String = req.body
    IngredientService.findById(id).then(
        (x: Ingredient) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function modifyIngredient(req, res, next) {
    let i: Ingredient = req.body

    IngredientService.save(i)
        .then((x: Ingredient) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function deleteIngredient(req, res, next) {
    let i: Ingredient = req.body
    IngredientService.save(i)
        .then((x: Ingredient) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
};
export async function getAllIngredient(req, res, next) {
    IngredientService.findAll().then((x: Ingredient[]) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}
