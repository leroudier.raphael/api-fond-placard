import { Recipe } from '../business/Recipe'
import { RecipeService } from '../services/RecipeService';

export async function createRecipe(req, res, next) {

    let x: Recipe = req['body']['name']
    RecipeService.save(x).then(
        (x: Recipe) => {
            res.status(201).json(x);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}

export async function getOneRecipe(req, res, next) {
    const id: String = req.body

    RecipeService.findById(id).then(
        (x: Recipe) => {
            res.status(200).json(x);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
}

export async function modifyRecipe(req, res, next) {
    let i: Recipe = req.body

    RecipeService.save(i)
        .then((x: Recipe) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function deleteRecipe(req, res, next) {
    let i: Recipe = req.body

    RecipeService.save(i)
        .then((x: Recipe) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function getAllRecipe(req, res, next) {

    RecipeService.findAll()
        .then((x: Array<Recipe>) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}

export async function search(req, res, next) {
 //   rechecher une query en fi=onction des ingrdient
    let body: string = req.body
    RecipeService.search(body)
        .then((x: Array<any>) => { res.status(200).json(x) })
        .catch((error) => { res.status(400).json({ error: error }) })
}