import { Db, MongoClient } from "mongodb";

//const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const config = require('config')

// Connection URL
//const url = 'mongodb://localhost:27017';

// Database Name
//const dbName = 'Sycatec';

//Options
var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 20
};

export function getConnection(): Promise<Db> {
    return new Promise(async function (resolve, reject) {
        MongoClient.connect(config.mongo.url, options, function (err, client) {
            assert.equal(null, err, 'Connect MongoDB Error');
            console.log("Connected successfully to MongoDB");
            const db = client.db(config.mongo.base);
            resolve(db)
        });

    })
}

