import { ObjectId } from "mongodb";
import { Ingredient } from "../business/Ingredient";
import { IngredientDAO } from "../datas/IngredientDAO";

export class IngredientService {
    static save(input: Ingredient): Promise<Ingredient> {
        return new Promise(async function (resolve, reject) {
            try {
                let ingredient = new Ingredient();
                input._id ? ingredient.setId((input._id)) : ingredient.setId(ingredient._id)
                ingredient.setName(input.name)
                const orm = new IngredientDAO()
                orm.save(ingredient)
                    .then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static delete(id): Promise<String> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new IngredientDAO()
                orm.deleteById(new ObjectId(id)).then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static findAll(): Promise<Ingredient[]> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new IngredientDAO()
                orm.findAll().then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static findById(id): Promise<Ingredient> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new IngredientDAO()
                orm.findById(new ObjectId(id)).then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
}
