import { ObjectId } from "mongodb";
// import { Ingredient } from "../business/Ingredient";
import { Recipe } from "../business/Recipe";
import { RecipeDAO } from "../datas/RecipeDAO";

export class RecipeService {
    static save(input): Promise<Recipe> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new RecipeDAO()
                let tab: Set<Recipe> = new Set([...input.ingredients])
                let recipe = new Recipe(tab);
                recipe.setId(input.id)
                recipe.setName(input.id)
                recipe.setCategory(input.id)
                recipe.setPicture(input.id)
                recipe.setScore(input.id)
                orm.save(recipe)
                    .then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static delete(id): Promise<String> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new RecipeDAO()
                orm.deleteById(new ObjectId(id)).then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static findAll(): Promise<Recipe[]> {
        return new Promise(async function (resolve, reject) {
            try {
                const orm = new RecipeDAO()
                orm.findAll().then((x) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static findById(id): Promise<Recipe> {
        return new Promise(async function (resolve, reject) {
            try {
                if (!ObjectId.isValid(id)) throw new Error("id invalide");
                id = typeof id == 'string' && ObjectId.isValid(id) ? new ObjectId(id) : id
                const orm = new RecipeDAO()
                orm.findById(new ObjectId(id)).then((x: Recipe) => { resolve(x) })
            } catch (err) {
                reject(err)
            }
        })
    }
    static like(id: string): Promise<any> {
        return new Promise(async function (resolve, reject) {
            try {
                let temp: any = ObjectId.isValid(id) && typeof 'String' ? new ObjectId(id) : id
                if (!temp || ObjectId.isValid(temp)) throw new Error("erreur d'id");
                const orm = new RecipeDAO()
                let recette = await orm.findById(temp)
                if (!recette) throw new Error("recette not found");
                let score: number = recette.getScore()
                recette.setScore(score--)
                await orm.save(recette).then(() => {
                    resolve(score)
                })
            } catch (err) {
                reject(err)
            }
        })
    }
    static dislike(id: string): Promise<any> {
        return new Promise(async function (resolve, reject) {
            try {
                let temp: any = ObjectId.isValid(id) && typeof 'String' ? new ObjectId(id) : id
                if (!temp || ObjectId.isValid(temp)) throw new Error("erreur d'id");
                const orm = new RecipeDAO()
                let recette = await orm.findById(temp)
                let score: number = recette.getScore()
                recette.setScore(score++)
                await orm.save(recette).then(() => {
                    resolve(score)
                })
            } catch (err) {
                reject(err)
            }
        })
    }
    static search(query: string): Promise<Recipe[]> {
        //
        //creer un agregate regex inssensible case
        return new Promise(async function (resolve, reject) {
            try {

            } catch (err) {

            }
        })
    }
}
