import { ObjectId } from "mongodb"

export default interface IngredientInterface {
    //GETTERS
    getId(): ObjectId
    getName(): string
    // SETTER
    setId(id: ObjectId): void
    setName(name: String): void
}