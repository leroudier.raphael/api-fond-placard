import { ObjectId } from "mongodb";
import { Ingredient } from "./Ingredient";

export class Recipe {
    _id?: ObjectId
    name: string
    category: string
    picture: string
    score: number
    ingredients?: Set<Ingredient>
    constructor(ingredients: Set<Ingredient>) {
        this._id = new ObjectId()
        this.name
        this.category
        this.picture
        this.score = 0
        this.ingredients = ingredients
    }
    //GETTERS
    getId(): ObjectId { return this._id }
    getName(): string { return this.name }
    getCategory(): string { return this.category }
    getPicture(): string { return this.picture }
    getScore(): number { return this.score }
    getIngredients(): Set<Ingredient> { { return this.ingredients } }
    // SETERS
    addIngredient(ingredient: Ingredient): void { { this.ingredients.add(ingredient) } }
    deleteIngredient(ingredient: Ingredient): void { this.ingredients.delete(ingredient) }
    setId(id: ObjectId): void {
        if (!ObjectId.isValid(id)) this._id = new ObjectId()
        this._id = ObjectId.isValid(id) && typeof 'string' ? new ObjectId(id) : id
    }
    setName(nom: string): void { this.name = nom }
    setCategory(category: string): void { this.category = category }
    setPicture(picture: string): void { this.picture = picture }
    setScore(score: number): void { this.score = score }

}