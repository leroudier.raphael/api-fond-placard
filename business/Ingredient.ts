import { ObjectId } from "mongodb"
import IngredientInterface from "./IngredientInteface"

export class Ingredient implements IngredientInterface {
    _id?: ObjectId
    name: string
    constructor() {
        this._id = new ObjectId()
        this.name
    }
    // GETTERS
    getId(): ObjectId { return this._id }
    getName(): string { return this.name }

    // SETTER
    setId(id: ObjectId): void {
        if (!ObjectId.isValid(id)) this._id = new ObjectId()
        this._id = ObjectId.isValid(id) && typeof 'string' ? new ObjectId(id) : id
    }
    setName(name: string): void { this.name = name }
}