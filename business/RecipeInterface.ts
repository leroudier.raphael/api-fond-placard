import { ObjectId } from "mongodb"

export interface RecipeInterface {
    //GETERS
    getId(): ObjectId
    getName(): String
    getCategory(): String
    getPicture(): String
    getScore(): Number
    // SETERS
    setId(id: ObjectId): void
    setName(nom: String): void
    setCategory(category: String): void 
    setPicture(picture: String): void 
    setScore(score: Number): void

}