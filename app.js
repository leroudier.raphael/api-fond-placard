const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const ingredientRoutes = require('./routes/ingredientRoutes');
const recipeRoutes = require('./routes/ingredientRoutes');

const app = express();
const connexion = require('./configMongo')
const assert = require('assert');
app.use(bodyParser.json());
const config = require('config');

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
// app.use('/api/ingredient', ingredientRoutes);
// app.use('/api/Recipe', recipeRoutes);


// // Connection URL
// var url = 'mongodb://localhost:50000,localhost:50001/myproject';
// // Use connect method to connect to the Server passing in
// // additional options
// MongoClient.connect(url, {
//     poolSize: 10, ssl: true
// }, function (err, db) {
//     assert.equal(null, err);
//     console.log("Connected correctly to server");
//     db.close();
// });



module.exports = app;


