
import { ObjectId } from 'mongodb';
import { Ingredient } from '../business/Ingredient';
import { getConnection } from '../configMongo';
import { DAO } from './DAO_Interface';


export class IngredientDAO implements DAO {
    findAll(): Promise<Ingredient[]> {
        return new Promise(async function (resolve, reject) {
            try {
                const db = await getConnection()
                let response = await db.collection('Ingredient').find({}).toArray()
                if (!response) throw new Error("requête non trouvée");
                resolve(response)
            } catch (err) {
                reject(err)
            }
        })
    }

    findById(id: ObjectId): Promise<Ingredient> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()

                let response: any = await db.collection('Ingredient').findOne({ name: 'Poivron' })
                if (!response) {throw new Error("requête non trouvée")}else{
                    resolve(response)
                }
              
            } catch (err) {
                reject(err)
            }
        })
    }

    save(input: Ingredient): Promise<Ingredient> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                let i: Ingredient = new Ingredient()
                let regex: RegExp = new RegExp(["^", input.name, "$"].join(""), "i");
                let find: Ingredient = await db.collection('Ingredient').findOne({ name: regex })
                if (find) i.setId(find['_id'])
                i.setName(input.name)
                await db.collection('Ingredient').replaceOne({ _id: i._id }, i, { upsert: true })
                resolve(i)
            } catch (err) {
                reject(err)
            }
        })
    }

    deleteById(id: ObjectId): Promise<String> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                await db.collection('Ingredient').deleteOne({ _id: id })
                resolve('Ingredient Deleted')
            } catch (err) {
                reject(err)
            }
        })
    }
}
