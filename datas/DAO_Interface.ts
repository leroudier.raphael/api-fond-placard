
import { ObjectId } from 'mongodb';

export interface DAO {
    findAll(): Promise<Object[]>
    findById(id): Promise<Object>
    save(input: Object): Promise<Object>
    deleteById(id: ObjectId): Promise<String>
}
