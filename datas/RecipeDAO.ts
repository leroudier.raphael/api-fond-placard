
import { ObjectId } from 'mongodb';
import { Recipe } from '../business/Recipe';
import { getConnection } from '../configMongo';
import { DAO } from './DAO_Interface';


export class RecipeDAO implements DAO {
    findAll(): Promise<Recipe[]> {
        return new Promise(async function (resolve, reject) {
            try {
                const db = await getConnection()
                let response = await db.collection('Recipe').find({}).toArray()
                if (!response) throw new Error("requête non trouvée");
                resolve(response)
            } catch (err) {
                reject(err)
            }
        })
    }

    findById(id: ObjectId): Promise<Recipe> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                let respons: any = await db.collection('Recipe').findOne({ _id: id })
                if (!respons) throw new Error("requête non trouvée");
                resolve(respons)
            } catch (err) {
                reject(err)
            }
        })
    }

    save(input: Recipe): Promise<Recipe> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                let r: Recipe = new Recipe(new Set([...input.ingredients]))
                let regex: RegExp = new RegExp(["^", input.name, "$"].join(""), "i");
                let find: Promise<any> = await db.collection('Recipe').findOne({ name: regex })
                if (find) r.setId(find['_id'])
                r.setCategory(input.category)
                r.setName(input.name)
                r.setPicture(input.picture)
                r.setScore(input.score)
                if (input._id) r.setId(input._id)
                await db.collection('Recipe').replaceOne({ _id: input._id }, r, { upsert: true })
                resolve(r)
            } catch (err) {
                reject(err)
            }
        })
    }

    deleteById(id: ObjectId): Promise<String> {
        return new Promise(async (resolve, reject) => {
            try {
                const db = await getConnection()
                await db.collection('Recipe').deleteOne({ _id: id })
                resolve('Recipe Deleted')
            } catch (err) {
                reject(err)
            }
        })
    }
}
