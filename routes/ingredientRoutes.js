const express = require('express');
const ingredientCtrl = require('../controllers/ingredientController');
const router = express.Router();

router.get('/', ingredientCtrl.getAllIngredient());
router.post('/', ingredientCtrl.createIngredient());
router.get('/:id', ingredientCtrl.getOneIngredient());
router.put('/:id', ingredientCtrl.modifyIngredient());
router.delete('/:id', ingredientCtrl.deleteIngredient());

module.exports = router;