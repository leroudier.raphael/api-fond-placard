const express = require('express');
const recipeCtrl = require('../controllers/recipeController');
const router = express.Router();

router.get('/', recipeCtrl.getAllRecipe());
router.post('/', recipeCtrl.createRecipe());
router.post('/:id', recipeCtrl.getOneRecipe());
router.put('/:id', recipeCtrl.modifyRecipe());
router.delete('/:id', recipeCtrl.deleteRecipe());
router.post('/:id/like', recipeCtrl.like());
router.post('/:id/dislike', recipeCtrl.dislike());
router.post('/search', recipeCtrl.Search());

module.exports = router;