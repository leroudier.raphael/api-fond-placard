import { ObjectId } from 'mongodb';
import { Ingredient } from '../business/Ingredient';
import { IngredientDAO } from '../datas/IngredientDAO';
import { Recipe } from '../business/Recipe';
import { RecipeDAO } from '../datas/RecipeDAO';
import { getConnection } from '../configMongo';
const MongoClient = require('mongodb').MongoClient
let Server = require('mongodb').Server

var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
const assert = require('assert');
let DB
// // Connection URL
// const url = 'mongodb://localhost:27017';

// // Database Name
// const dbName = 'myproject';
// before((done) => {
//   MongoClient.connect(url, function (err, client) {
//     assert.equal(null, err);
//     console.log("Connected successfully to server");

//     DB = client.db(dbName);

//     client.close();
//   });
//   done();
// });
// Use connect method to connect to the server



//variable pour les test

chai.use(chaiHttp);

describe('TEST', async function () {
  beforeEach((done) => {
    done();
  });
  let x
let z
  //import {Ingredients} from '../business/IngredientInterface'
  describe('test d\'ingredient', function () {

    it('instance d\'un ingredient', function () {

      x = new Ingredient()
      x.setName('Piment')
      let y = x.getName()
      y.should.be.eql('Piment')
      let id = x.getId()
      id.toHexString().should.be.eql(x._id.toHexString())
    });

    it('save d\'un ingredient apres modif du name', async function () {

      const orm = new IngredientDAO()
      x.setName('Poivron')
      x.name.should.be.eql('Poivron')
      await orm.save(x).then(w => {
        //   w.should.be.a('object')
        w.name.should.be.eql('Poivron')
      })

    });
    it('findById d\'un ingredient', async function () {
      const orm = new IngredientDAO()
      console.log('id', x._id.toHexString());
      let essai = await orm.findById(x._id)
      console.log(essai);


    });
    it('findAll d\'ingredients', async function () {
      const orm = new IngredientDAO()
      await orm.findAll()
        .then(y => {
          y.should.be.a('array')
          y[0].name.should.be.eql('Poivron')
        })

    });
    it('delete ingredient', async function () {
      const orm = new IngredientDAO()
      await orm.deleteById(x._id)
    });
  });
  await describe('test recipe', function () {

    it('instance d\'un Recipe', function () {
      z = new Recipe(new Set([]))
      z.addIngredient(x)
      z.setName('Pili-Pili')
      z.setCategory('Epicé')
      z.setPicture()
      z.setScore(1)
      z.name.should.be.eql('Pili-Pili')
      let test = z.getName()
      test.should.be.eql('Pili-Pili')
      test = z.getCategory()
      test.should.be.eql('Epicé')
      test = z.getScore()
      test.should.be.eql(1)
      test = z.getIngredients()
      //voir le type de Set
      console.log('type', typeof test);
      console.log('test', test);


    });
    it('save d\'un Recipe apres modif du name', async function () {
      const orm = new RecipeDAO()
      z.setName('Pily-Pily')
      z.name.should.be.eql('Pily-Pily')
      await orm.save(z).then(w => {
        w._id.should.be.eql(z._id)
      })
    });
    it('findById d\'un Recipe', async function () {
      const orm = new RecipeDAO()
      await orm.findById(z._id)
        .then(y => {
          y.name.should.be.eql('Piment')
        })
    });
    it('findAll d\'un recipe', async function () {
      const orm = new RecipeDAO()
      await orm.findAll()
        .then(s => {
          s.should.be.a('array')
          s.length.should.be.eql(1)
          s[0].name.should.be.eql(z.name)
        })
    });
    it('delete recipe', async function () {
      const orm = new RecipeDAO()
      let array = await orm.findAll()
      //verifier que array vide soit pas genant
      console.log(array)
    });

    //tester search

  })
})